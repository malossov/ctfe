# PWN简单引入

[TOC]

> 由于本人也很菜，而且不能说自己对于PWN的知识有着系统性、完全的学习和理解，因此在这里**基本上以放链接为主**。**本次招新也全部为栈题**，不会涉及更需要分析能力的堆题。难度不大。**加油！期待你们创我门。**

## 什么是PWN？

- 关于定义：[从零开始入门pwn(一)：pwn的介绍以及部分前置知识-CSDN博客](https://blog.csdn.net/m0_51466347/article/details/121742858)
  - 文章说的比较玄乎，其实简单来说，PWN就是**通过二进制文件漏洞拿shell**：
    - A[大黑客**拿到程序**</br>顺便把本地环境配置的和服务器一样]-->B[大黑客**简单看一下程序的基本信息**/防护信息、符号表等]-->C[大黑客**开始逆向**了</br>开始使用gdb/ida进行**动态调试**]-->D[通过逆向静态观察</br>动态调试**发现程序的漏洞**]-->E[大黑客**撰写攻击脚本**]-->F[大黑客**发动脚本,就在一瞬间,就进了SHELL**]
      - 当然，其实大部分时间都是在D到E之间往复循环，很多时候，如果不动态调试、预期和实际可能也存在差距。
  - 和RE的区别：个人观点：RE更强调加解密、绕各种限制;PWN更强调程序运行分析、漏洞利用。

## 入坑PWN需要做什么

### 前置知识

- 所以，学习PWN，需要做的便是：
  - 学习**计算机操作系统**的基础知识——目前主要是LINUX的基础知识
    - 推荐使用WSL
  - 学习**汇编语言**
    - 我也不是很会。~~其实，对付招新题基本上只要看懂gdb在干啥就行x~~
    - 这块主要留意一下：
      - i386和AMD64之间的区别：函数如何调用、栈空间如何进行放置
    - 学习LINUX下的**程序编译到运行**的过程、中间可以利用的弱点。
  - 学会**逆向分析和程序调试**的工具。
    - 这个群里面我发的书《CTF竞赛权威指南PWN》讲的比较多，可以看看哦。
    - 补充内容见下。
  - **攻击脚本**反而只是三者的结合。：因此，在入门时、**不要急着想打环境！！** 
    - 在正式开打前、先详细的了解知识点才是正道。
    - ~~当然、如果需要熟悉工具的话，还是先做一下==Yu者斗Flag龙==~~吧

### 怎样入门

- 相关知乎问题给出了很多很优秀的回答：
  - [如何一步一步学好pwn？ - 知乎 (zhihu.com)](https://www.zhihu.com/question/312599762)
  - [如何入门pwn？ - 知乎 (zhihu.com)](https://www.zhihu.com/question/464671097)
- 摘抄回答：
  - 入门的话，从栈溢出开始做（就是我们的LAB里面的题目），
  - 不懂某些原理可以用那本书相应的知识去补足这个空缺。
  - 养成写博客的好习惯，**写实验报告，写wp**（我们的平台就给了你充足的机会去IE），发表疑问都可以。
    - ==一定要养成记录的习惯！！！！！！！！==

## 前置知识补充

[Never Gonna Give You Up - Rick Astley](https://music.163.com/song?id=5221167)

- ~~首先恭喜你被我骗了~~
- 群文件书就是最好的知识合集，可以当字典查的那种。
- 只要精读前6章/前139页，就是最好的啦。
- ~~其实更推荐边做边看，但是↓下面的工具要好好利用哦。~~

### 具体补充

1. Linux相关：
   1. [Linux 系统启动过程 | 菜鸟教程 (runoob.com)](https://www.runoob.com/linux/linux-system-boot.html)：可以看看：启动、系统目录结构、文件相关、用户相关、vim、一些常见命令
   2. ==在招新平台上有题哦！推荐先去看看题目怎么说！==
   3. [Arch Linux 中文维基 (archlinuxcn.org)](https://wiki.archlinuxcn.org/wiki/首页)：虽然不推荐新人用ARCH，但是ARCH-WIKI中的知识、很多时候也能解决其他发行版的问题-->不如先来看看ssh吧。
2. DNK推荐的：[程序员的自我修养—链接、装载与库.pdf · younisd/CS-Books - 码云 - 开源中国 (gitee.com)](https://gitee.com/younisd/CS-Books/blob/master/程序员的自我修养—链接、装载与库.pdf)：能够帮你更好的理解什么是动态链接、什么是库。
3. 汇编知识这块：
   1. 看书上的基本也够。
   2. 有兴趣可以看看：[FriendLey/assembly-language-learning: 汇编语言学习：使用王爽写的《汇编语言》第三版 (github.com)](https://github.com/FriendLey/assembly-language-learning)
      1. 万变不离其宗吧。但是需要足够的耐心，如果只是打题，用处不是特别特别大。

## 工具

> 只提供工具名称和方法等、但是本人不专业无法提供教程私密马赛。
>
> ~~学了记得提交到平台上教我。~~

### 三大件

- 其实三大件装上就够了：
  - [从零开始入门pwn(二):环境搭建_pwn环境搭建_小白之耻的博客-CSDN博客](https://blog.csdn.net/m0_51466347/article/details/121851783)
  - 或者：[pwn 环境搭建（wsl/vmware） - PwnKi - 博客园 (cnblogs.com)](https://www.cnblogs.com/luoleqi/p/14165817.html)：我自己用的差不多也是这个
- 哪三大件？
  - IDA7.7：[IDA Pro 7.7.220118 (SP1) 全插件绿色版 - 『逆向资源区』 - 吾爱破解 - LCG - LSG |安卓破解|病毒分析|www.52pojie.cn](https://www.52pojie.cn/forum.php?mod=viewthread&tid=1584115)
  - GDB：根据发行版安装，但是我强烈安利：[D1ag0n-Young/gdb321: pwndbg、pwn-peda、pwn-gef和Pwngdb四合一，一合四，通过命令gdb-peda、gdb-pwndbg、gdb-peda轻松切换gdb插件 (github.com)](https://github.com/D1ag0n-Young/gdb321)能装上基本上所有插件。
  - pwntools:[pwntools — pwntools 4.13.0dev documentation](https://docs.pwntools.com/en/latest/)
    - 配合：[Gallopsled/pwntools-tutorial: Tutorials for getting started with Pwntools (github.com)](https://github.com/Gallopsled/pwntools-tutorial)
    - 需要注意使用的python版本问题。
    - 快速生成pwn脚本和环境的工具：[pwninit - crates.io: Rust Package Registry](https://crates.io/crates/pwninit/3.0.2)

### 其他工具

#### 二进制文件工具

~~这个挺多的~~

- pwntools自带的就够玩了
  - checksec
  - ROPgadget
- patchelf:推荐从包管理器下载：
- 还有一堆ruby写的gem安装的，网上搜搜很多。

#### libc收集

- [niklasb/libc-database: Build a database of libc offsets to simplify exploitation (github.com)](https://github.com/niklasb/libc-database)
  - 比起它的github，我更推荐：[libc-database](https://libc.rip/)它的网页端神器
- [matrix1001/glibc-all-in-one: 🎁A convenient glibc binary and debug file downloader and source code auto builder (github.com)](https://github.com/matrix1001/glibc-all-in-one)：非常好仓库，阅读readme之后开始下载。

#### AWD方向

- AWD攻击的脚本：[i0gan/awd_script: AWD批量攻击脚本(Web/Pwn通用)，通过bash编写，远程信息采用参数传入exp，通过多进程方式实现同时攻打，阻塞超时自动结束进程。 (github.com)](https://github.com/i0gan/awd_script)
  - 其中关于PWN脚本的模板也可以看看。
- AWD的WAF脚本：[i0gan/pwn_waf: This is a simple network firewall for pwn challenges of ctf awd competition, light and simple code.There is no dependence, the log format is clear with the hexadecimal payload string and original string, which is more convenient to exp script. 【ctf awd比赛中的针对于pwn题的waf，拥有抓取、通防、转发、多人转发模式，用起来超棒】 (github.com)](https://github.com/i0gan/pwn_waf)
  - 嗯。超级棒。

## 漏洞学习

- [Environment - CTF Wiki (ctf-wiki.org)](https://ctf-wiki.org/pwn/linux/user-mode/environment/)：著名的中文CTF-Wiki，左侧重点知识点进去就行了。推荐的学习顺序是：**栈溢出-->格式化字符串-->堆**，各种调试技巧等都是在不断学习中看到的。~~感觉把栈溢出看完这招新剩余时长已经不够了~~
- [zhengmin1989/ROP_STEP_BY_STEP: 一步一步学ROP (github.com)](https://github.com/zhengmin1989/ROP_STEP_BY_STEP)：解决本次平台上的题目首选推荐、但是原博客找不到了，但是转载的很多。

- 著名的HOW2HEAP：[shellphish/how2heap: A repository for learning various heap exploitation techniques. (github.com)](https://github.com/shellphish/how2heap)
  - HOW2HEAP的中文翻译：[yichen115/how2heap_zh: 汉化加补充自己的理解 (github.com)](https://github.com/yichen115/how2heap_zh)

## 良莠不齐的入门资料

### 强烈推荐

- ==强烈推荐自己总结，自己总结就是最好的！==

### 一般推荐

- [[pwn基础\]PWN二进制漏洞学习指南 - VxerLee昵称已被使用 - 博客园 (cnblogs.com)](https://www.cnblogs.com/VxerLee/p/15424926.html)：资源很多，但是新手可能看不懂、到要学的时候看就行
- [AWDPwn 漏洞加固总结 - FreeBuf网络安全行业门户](https://www.freebuf.com/articles/web/283020.html)：AWD方向，真挺好。

### 不是很推荐

- [PWN入坑指南 - FreeBuf网络安全行业门户](https://www.freebuf.com/sectool/195251.html)：算是一个总览、看看漏洞都是什么。

- [超全PWN入门笔记，从栈到堆一步到位 - X0H3M1 - 博客园 (cnblogs.com)](https://www.cnblogs.com/X0H3M1/articles/16669128.html)：超全个鬼，但是可以进来补补名词，防止看不懂其他人写的）
- [Linux pwn入门学习到放弃 - FreeBuf网络安全行业门户](https://www.freebuf.com/vuls/248330.html)：主要是关于LINUX栈保护和工具收集的
- [pwn_AWD自动化脚本 - 星雪亭 (xineting.github.io)](https://xineting.github.io/2018/12/07/pwn_AWD自动化脚本/)：AWDPWN的脚本，写的一般
- [[CTF PWN\] 从0到0.00001 PWN入门超级详细_ctfpwn入门-CSDN博客](https://blog.csdn.net/weixin_45004513/article/details/117332121)

## 招新小平台说明

1. 招新小平台获取的flag为：`YulinPwn{xxxxx}`
2. 通过将小平台的flag提交给`YulinPWNFlag姬`即可查看提交到大平台上的flag。
   1. 初步设定为每个分类有一个flag。
   2. 因此你完成一块、并在小平台验证之后，才能在大平台获得1个flag。
3. 玩得愉快！学不完小平台服务器也是可以继续开着的！

## 致歉

- 对不起，本人不是什么专业的PWN爷，甚至会比大家学了一点之后还要更菜。
- 能做的只有好好整理一下根据现有经验觉得不错的东西。
- 希望大家能赏个脸读一下，如果有坑也创一下我。
- 下面是已阅证明：
- `YulinPwn{7he_W0rst_Issuer_MALossov}`